
## Bad performance when reading the short-term table

Load-from-cassandra pulls all the rows onto the cluster. This affects the long-term aggregation and the generation of squares reports.

Aggregator call sequence:

* `cenx.plataea.cassandra.long-term.aggregate/newest-rows`
    * loads short-term rows
    * filters out rows older than the latest aggregated time
    * (CD-47646) finds for the earliest timestamp then filters out rows older than the earliest timestamp
* `cenx.plataea.cassandra.vnf.rdd/load-from-cassandra`
* `cenx.prometheus.spark.cassandra/table->rdd`

Solution idea: call `.where` [1] after the call to `table->rdd` to put a where clause on the RDD.

**Does the filtering of an RDD containing a where clause happen in the driver?**

Apparently not. CassandraTableScanRDD uses the argument of `.where` [2] to create a query string [3] and sends it to the database [4].

### Filtering for aggregator and squares

The aggregator uses the vdouble writetime to filter the data. The squares generator uses the sample timestamp.

It seems it is not possible to use writetime in the where clause of a query [5], because it is not a part of the table's primary key. It is metadata attached to a value in each row.

### Solution options

(1) No schema change: cassandra requires us to specify the metrics before we specify a time range in the where clause, so we need to find out what metrics to use. We can:

*  look at samples during ingestion to get their metric names;
*  look at the custom docs to get names of metrics that have graphs and square reports.
    *  Currently we calculate squares on all metrics. We could load only the ones listed in the squares entity. [6]
    *  It would be good to have a list of metrics to be aggregated in the docs. At the moment, names of metrics for analytics graphs appear in mapping directives (mapping.js) and in chart specs (ui.js).
*  (for the aggregator) request all ids in the short-term table then create one aggregation job for each id, with a longer time range.
    *  A small deployment with levski and cassandra on the same machine and levski and spark on the same JVM aggregated one hour of data from a table containing twelve hours of samples (15k samples/sec) in 41 min 20 sec. The current implementation running on the same system could not aggregate ten minutes of data at the same rate.

(2) Schema change:

* Modify the clustering key: put timestamp before metric;
    * modify vnf and long-term.query;
    * modify levski if necessary;
    * this solves the problem for squares but not for aggregator, because aggregator filters with writetime. One way to use this idea to solve for aggregator would be to have a separate table in which a cenx-timestamp column replaces the (sample) timestamp in the clustering key.
    * The small deployment on the laptop aggregated one hour of data in 20 min 44 sec. Note that the timestamp searches were disabled.
    * An amun-cluster job created by dev-17 took 13 min 41 sec to aggregate one hour of data from a one-hour table [7]. A job that aggregated one hour of data from a four-hour table took 13 min 19 sec [15]. The similar times show that `.where` makes the database filter the rows.
    * The query function in delorean needs a change to extract the requested metric from a result set that will contain all metrics in the range. Because Cassandra refuses a query with a constraint by inequality followed by a constraint by equality:

```
$ cqlsh -e "select count(*) from upd_cl_key_5_min.vnf_data where id='id0' \
> and idfamily='vim' and week=2517 and timestamp >= 1522627200000 and timestamp < 1522627320000 \
> and metric='metric01';"
<stdin>:1:InvalidRequest: code=2200 [Invalid query] message="Clustering column "metric" cannot be restricted
(preceding column "timestamp" is restricted by a non-EQ relation)"
```

* Move metric to partition key.
    * Partition key now is (id, id-family, week). This means all metrics for one id are in the same location.
    * If the partition key changes to (id, id-family, week, metric), all samples for each id-metric combination will be in the same location.
    * Probably more work than the change to the clustering key.
    * No impact on how the aggregator operates: it should filter the time range. It needs to get all ids and metrics.
    * Simplifies the spark code that calculates squares.
    * A job that aggregated one hour of data from a four-hour table containing 25K samples for each second took 24 min 42 sec [8].

Migration in case of schema change:

* export existing short-term data;
* drain short-term: aggregate everything in it, then remove the table.

Reduce aggregation time by replacing the timestamp searches:

*  It's possible to eliminate the search for the earliest sample timestamp by using (sample) time ranges that begin and end at the top of the hour.
*  A way to deal with delayed data is to set the end of the time range to be a number of hours before the beginning of the current hour (similar to the squares' grace period).
*  For the case of a backlog of samples in the short-term table, we can stipulate a configurable number of hours to be aggregated in the first run.

(3) Out-of-the-box ideas:

*  There is a way to update a list of values in a Cassandra row [9]. Maybe it would be a good solution to remove the aggregator and write directly to the long-term table.

```
create keyspace test with replication =
  {'class': 'SimpleStrategy', 'replication_factor': 1};

create table test.my_table (id bigint,
  elements set<frozen<tuple<bigint, double>>>,
  primary key (id));

insert into test.my_table (id, elements) values (1, { (1000, 10.0) });

update test.my_table set elements = elements + {(1001, 10.1)} where id = 1;
```

*  Would it be possible to have an aggregation process that uses kafka as input instead of the short-term table?


### Tests of the current implementation on the amun cluster

Total time is what the repl reports when the aggregate call completes.

```
user> (time (cd48222.levski-repl/aggregate "ten_minutes"))
"Elapsed time: 1647723.431952 msecs"
```

Time, input and output columns come from the spark web console.  
(1) go to [http://cassandra01-amun.cenx.localnet:7080/](http://cassandra01-amun.cenx.localnet:7080/)  
(2) select "dev17 Analytics spark calculations" under "Running Applications"  
(3) select "Stages" on the top bar  

**Ten minutes table (9M rows - 15K samples/sec)**  
total time: 27min 28s

| stage | operation               | location                 | time  | input | output |
|-------|-------------------------|--------------------------|-------|-------|--------|
| 1     | find-earliest-timestamp | newest-rows              | 14min | 414M  |        |
| 2     | save-to-cassandra       | populate-long-term-table | 14min | 414M  | 138M   |
| 3     | find-latest-timestamp   | populate-long-term-table | 0.9s  | 4.3G  |        |

**Thirty minutes table (27M rows)**  
total time: 29min 5s

| stage | operation                    | time (min) | input | output |
|-------|------------------------------|------------|-------|--------|
| 1     | find-earliest-timestamp [10] | 12         | 1247M |        |
| 2     | save-to-cassandra [11]       | 13         | 1242M | 414M   |
| 3     | find-latest-timestamp [12]   | 3.5        | 8.9G  |        |

**One hour table (54M rows)**  
total time: 1h 0min 59s

| stage | operation               | time (min) | input | output |
|-------|-------------------------|------------|-------|--------|
| 1     | find-earliest-timestamp | 26         | 2.4G  |        |
| 2     | save-to-cassandra       | 23         | 2.4G  | 828M   |
| 3     | find-latest-timestamp   | 12         | 10.5G |        |

**Four hours table (216M rows)**

| stage | operation               | time (min) | input | output      |
|-------|-------------------------|------------|-------|-------------|
| 1     | find-earliest-timestamp | 46         | 6.9G  | failed [13] |

It's possible to eliminate stage 1 by using the merge-with-long-term solution for CD-47646 [14].

Question: why is the input size for the find-latest-timestamp stage larger?

### Tests of the clustering-key solution on the amun cluster

Jobs ran on 3 cores. Each core had 6 GB of memory.

| test | samples/s | rows/h | time (min:sec) | input  | output    | screenshot |
|------|-----------|--------|----------------|--------|-----------|------------|
| 1    | 15K       | 54M    | 13:19          | 2.0 GB | 829.1 MB  | [15]       |
| 2    | 25K       | 90M    | 21:22          | 3.4 GB | 1381.8 MB | [16]       |
| 3    | 35K       | 126M   | 33:49          | 4.8 GB | 1934.5 MB | [17]       |
| 4    | 45K       | 162M   | 36:21          | 6.1 GB | 2.4 GB    | [18]       |
| 5    | 55K       | 198M   | 44:50          | 7.5 GB | 3.0 GB    | [19]       |
| 6    | 65K       | 234M   | 53:32          | 8.9 GB | 3.5 GB    | [20]       |

Deltas between successive tests:

*   sample rate increased by 10K
*   num rows increased by 36M
*   input size changed by 1.4 GB
*   output size changed by 552.7 MB

| test | time delta |
|------|------------|
| 1->2 | 483 s      |
| 2->3 | 747 s      |
| 3->4 | 152 s      |
| 4->5 | 509 s      |
| 5->6 | 522 s      |

Tests 1,2 and 3 read one hour of data from tables that had four hours. Tests 4, 5 and 6 read from tables having only one hour. The time to write four hours for test 3 was almost six hours.

### Tests with a 3.5M samples/min table on the amun-ssd cluster

| num cores | time (mm:ss) | screenshot |
|-----------|--------------|------------|
| 3         | 56:36        | [21]       |
| 18        | 11:35        | [22]       |
| 24        | 09:41        | [23]       |
| 36        | 06:18        | [24]       |

The table is in the amun-ssd cluster:

*   cassandra01-amun-ssd.cenx.localnet
*   keyspace cd41364_perf_util_1d_ttl0
*   table short_term

It has one day of data structured as:

*   (performance data) 72,917 sites × 4 ids, each with 9 metrics, each metric has one sample per minute
*   (utilization data) 72,917 sites × 6 ids, each with 10 metrics, each metric has one sample every 5 minutes

See the analytics data size requirement CD-41364 [25].

The number of rows is 5,040,023,040. Equals 3,500,016 times 1,440 (number of minutes in the day).

Dev28 took 41:44:30 to write the performance data and 13:20:16 to write the utilization data. The code to create the table is in commit 063c6bf from 2018-06-11.

The values in the rows change according to the metric name.

| performance metrics     | value |
|-------------------------|-------|
| min-fl, min-fd, min-fdv | 10.0  |
| avg-fl, avg-fd, avg-fdv | 55.0  |
| max-fl, max-fd, max-fdv | 100.0 |

| utilization metrics | value |
|---------------------|-------|
| m01                 | 1.0   |
| m02                 | 2.0   |
| ...                 | ...   |
| m10                 | 10.0  |


### Profiling a spark job

Spark uses the metrics library [26]. The library provides measurement resources: counters, timers, histograms, health checks. These are objects that one adds to the code to be measured. There is a registry which one will use to obtain an object. One should also configure a sink for the metrics reports.

Spark provides REST endpoints (search "REST API" on [27]) for retrieving info on a job. Maybe some endpoint return reports for custom metrics (metrics I would add to the job). In any case, one of the endpoints returns logs, which are an available output choice in metrics-library.

List of possible (aggregation) sites for timers:

```
cenx.plataea.cassandra
  .long-term
    .aggregate/
      populate-long-term-table (1)
      aggregate (2)
      aggregate-rows-with-same-id (3)
      update-short-term-row-map (4)
      build-value-list (4)
    .base/value-list->byte-array-macro (4)
  .rdd/save-to-cassandra (2)
  .vnf.rdd/load-from-cassandra (2)
```

Numbers enclosed in parentheses indicate the call stack depth.

### Migrating VCP data into a table with the clustering-key change

Mostefa and Leonardo met with Joel on 2018-05-02 to talk about ways to execute the migration. Joel suggested using the Spark cluster. This will probably be the best way to migrate the existing EBH data.

Mostefa, Mahmood and Leonardo met with Serge, Jason, Steve, Shawn and Aaron on 2018-05-03 to talk about migration of the VCP data.

We learned that VCP has around 100 VNFs. Each one generates 1 sample per minute.

Jason asked us to engineer for one more order of magnitude. This means we should expect 1000 VNFs.

Number of rows in 10 days:

10 days * 1000 id-metrics / minute =  
= 10 * 24 * 60 minutes * 1000 id-metrics / minute  
= 14.4M id-metrics or rows.

On the test with a one hour table at 25K samples/s on the laptop, the migration program finished 14.472M rows in 19 min. The entire table (90M rows) took 2 h 5 min.

### References

[1] doc about .where  
[https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/doc/3_selection.md#filtering-rows---where](https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/doc/3_selection.md#filtering-rows---where)

[2] CassandraRDD.where  
[https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/spark-cassandra-connector/src/main/scala/com/datastax/spark/connector/rdd/CassandraRDD.scala#L75](https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/spark-cassandra-connector/src/main/scala/com/datastax/spark/connector/rdd/CassandraRDD.scala#L75)

[3] CassandraTableScanRDD.tokenRangeToCqlQuery (this creates a query string)  
[https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/spark-cassandra-connector/src/main/scala/com/datastax/spark/connector/rdd/CassandraTableScanRDD.scala#L287](https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/spark-cassandra-connector/src/main/scala/com/datastax/spark/connector/rdd/CassandraTableScanRDD.scala#L287)

[4] CassandraTableScanRDD.fetchTokenRange (uses the result of tokenRangeToCqlQuery)  
[https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/spark-cassandra-connector/src/main/scala/com/datastax/spark/connector/rdd/CassandraTableScanRDD.scala#L327](https://github.com/datastax/spark-cassandra-connector/blob/v2.0.2/spark-cassandra-connector/src/main/scala/com/datastax/spark/connector/rdd/CassandraTableScanRDD.scala#L327)

[5] StackOverflow post: how to filter cassandra table by writetime  
[https://stackoverflow.com/questions/31184376/how-to-filter-cassandra-result-based-on-writetime](https://stackoverflow.com/questions/31184376/how-to-filter-cassandra-result-based-on-writetime)

[6] Squares entity wiki  
[https://cenx-cf.atlassian.net/wiki/spaces/KB/pages/220528934/Analytics+Square+Report](https://cenx-cf.atlassian.net/wiki/spaces/KB/pages/220528934/Analytics+Square+Report)

[7] Spark web console screenshot of the one-hour aggregation from one-hour table job.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-table.png)

[8] Spark web console screenshot of job that aggregated one hour from a four-hour table with 25K samples per second. The table had metric in the partition key.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/partition-key-1h-from-4h-25K-samples-sec-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/partition-key-1h-from-4h-25K-samples-sec-table.png)

[9] Cqlsh session showing an update to a list of values.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/cqlsh-update.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/cqlsh-update.png)

[10] Screenshot of the find-earliest-time stage on the spark web console. This stage reduces the RDD to obtain the earliest sample time (see CD-47646).  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/find-earliest-timestamp.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/find-earliest-timestamp.png)

[11] Screenshot of the save-to-cassandra stage on the spark web console. This is the stage where the aggregation work happens.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/save-to-cassandra.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/save-to-cassandra.png)

[12] Screenshot of the find-latest-timestamp stage on the spark web console. In this stage, spark reduces an RDD to find the latest writetime, which will be used the next time the aggregator runs.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/find-latest-timestamp.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/find-latest-timestamp.png)

[13] Exception thrown by the cluster while working on the four hours table  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/exception-from-run-with-four-hour-table.txt](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/exception-from-run-with-four-hour-table.txt)

[14] merge-with-long-term solution  
[https://bitbucket.org/cenx-cf/plataea/pull-requests/920/cd-47646-fix-by-joining-short-term-and/diff](https://bitbucket.org/cenx-cf/plataea/pull-requests/920/cd-47646-fix-by-joining-short-term-and/diff)

[15] Spark web console screenshot of the one-hour aggregation from four-hour table job.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-4h-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-4h-table.png)

[16] Spark web console screenshot of job that aggregated one hour from a four-hour table with 25K samples per second.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-4h-25K-samples-sec-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-4h-25K-samples-sec-table.png)

[17] Spark web console screenshot of job that aggregated one hour from a four-hour table with 35K samples per second.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-4h-35K-samples-sec-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-4h-35K-samples-sec-table.png)

[18] Spark web console screenshot of job that aggregated one hour from a one-hour table with 45K samples per second.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-45K-samples-sec-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-45K-samples-sec-table.png)

[19] Spark web console screenshot of job that aggregated one hour from a one-hour table with 55K samples per second.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-55K-samples-sec-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-55K-samples-sec-table.png)

[20] Spark web console screenshot of job that aggregated one hour from a one-hour table with 65K samples per second.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-65K-samples-sec-table.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/clustering-key-1h-from-1h-65K-samples-sec-table.png)

[21] Spark web console screenshot of job that aggregated one hour from the 3.5M samples/min table with 3 cores.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-03cores.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-03cores.png)

[22] Spark web console screenshot of job that aggregated one hour from the 3.5M samples/min table with 18 cores.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-18cores.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-18cores.png)

[23] Spark web console screenshot of job that aggregated one hour from the 3.5M samples/min table with 24 cores.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-24cores.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-24cores.png)

[24] Spark web console screenshot of job that aggregated one hour from the 3.5M samples/min table with 36 cores.  
[https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-36cores.png](https://bitbucket.org/leonardo_nobrega/cd48222/src/master/doc/3.5M-36cores.png)

[25] Analytics data size requirement.  
[https://cenx-cf.atlassian.net/browse/CD-41364](https://cenx-cf.atlassian.net/browse/CD-41364)

[26] Metrics getting-started.  
[http://metrics.dropwizard.io/4.0.0/getting-started.html](http://metrics.dropwizard.io/4.0.0/getting-started.html)

[27] Spark 2.0.2 monitoring.  
[https://spark.apache.org/docs/2.0.2/monitoring.html](https://spark.apache.org/docs/2.0.2/monitoring.html)
