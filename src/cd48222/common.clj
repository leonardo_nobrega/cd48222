(ns cd48222.common
  (:require [qbits.alia :as alia]
            [qbits.hayt :as hayt]))

;;; cassandra

(def table-name :short_term)

(def columns
  {:id :text
   :idfamily :text
   :week :bigint
   :metric :text
   :timestamp :bigint
   :vtext :text
   :vlong :bigint
   :vdouble :double
   :primary-key [[:id :idfamily :week] :metric :timestamp]
   :clustering-order [[:metric :asc]
                      [:timestamp :desc]]})

(def columns-with-updated-clustering-key
  (-> columns
      (assoc :primary-key [[:id :idfamily :week] :timestamp :metric])
      (assoc :clustering-order [[:timestamp :desc]
                                [:metric :asc]])))


(defn make-session
  [cassandra-address]
  (->> cassandra-address
       (hash-map :contact-points)
       alia/cluster
       alia/connect))

(defn make-session-obj
  ([]
   (make-session-obj "localhost"))

  ([cassandra-address]
   (atom {:session (make-session cassandra-address)
          :address cassandra-address})))

(defn get-session
  [session-obj]
  (-> session-obj deref :session))

(defn reconnect
  [session-obj]
  (swap! session-obj #(assoc %
                             :session
                             (-> session-obj deref :address make-session))))

(defn create-table
  ([session-obj keyspace table-name columns]
   (let [ttl-ten-days (* 10 24 60 60)]
     (create-table session-obj keyspace table-name columns)))

  ([session-obj keyspace table-name columns ttl]
   (let [keyspace-props {:replication {:replication_factor 1
                                       :class "SimpleStrategy"}}
         table-props (-> {:default_time_to_live ttl}
                         (assoc :clustering-order
                                (:clustering-order columns)))
         commands (list (hayt/create-keyspace keyspace
                                              (hayt/if-exists false)
                                              (hayt/with keyspace-props))
                        (hayt/use-keyspace keyspace)
                        (hayt/create-table table-name
                                           (hayt/if-not-exists)
                                           (hayt/column-definitions (dissoc columns
                                                                            :clustering-order))
                                           (hayt/with table-props)))]
     (println (map hayt/->raw commands))
     (->> commands
          (map (partial alia/execute (get-session session-obj)))
          dorun))))

(defn table-with-keyspace
  [keyspace-str table-name-kw]
  (keyword (str keyspace-str "." (name table-name-kw))))

(defn make-insert
  "row should be a map having the keys: id, idfamily, metric, timestamp, vdouble"
  [table-name row]
  (hayt/insert table-name (hayt/values row)))

(defn batch-of-row-inserts
  [batch-size table-name rows]
  (let [make-batch (fn [inserts]
                     (hayt/batch (apply hayt/queries inserts)
                                 (hayt/logged false)))]
    (->> rows
         (map (partial make-insert table-name))
         (partition batch-size batch-size nil)
         (map make-batch))))

;;; progress logs

(defn format-long-number
  [num]
  (let [format-parts (fn [parts]
                       (list* (-> parts first str)
                              (->> parts rest (map #(format "%03d" %)))))]
    (loop [current num
           parts (list)]
      (let [q (quot current 1000)
            new-parts (cons (rem current 1000) parts)]
        (if (> q 0)
          (recur q new-parts)
          (->> new-parts
               format-parts
               (interpose ",")
               (apply str)))))))

(defn time-ms->string
  [time-ms]
  (let [hour (quot time-ms (* 60 60 1000))
        minutes-in-ms (- time-ms (* hour 60 60 1000))
        minute (quot minutes-in-ms (* 60 1000))
        seconds-in-ms (- minutes-in-ms (* minute 60 1000))
        second (quot seconds-in-ms 1000)]
    (format "%02d:%02d:%02d" hour minute second)))

(defn log-progress
  [num-rows current-sample-time-ms start-time-ms]
  (let [elapsed-time-ms (- (System/currentTimeMillis) start-time-ms)]
    (println (format "%s rows written - sample time %s - elapsed %s"
                     (format-long-number num-rows)
                     (time-ms->string current-sample-time-ms)
                     (time-ms->string elapsed-time-ms)))))
