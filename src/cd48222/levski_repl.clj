(ns cd48222.levski-repl
  (:require [cenx.plataea.cassandra :as c]
            [cenx.plataea.cassandra.init :as ci]
            [cenx.plataea.cassandra.long-term.aggregate :as agg]
            [cenx.plataea.cassandra.long-term.base :as base]
            [cenx.plataea.cassandra.processing-states :as ps]
            [cenx.plataea.cassandra.session :as cs]))

;; load this onto a levski repl
;; (use cider-assoc-buffer-with-connection)

;; eval this on emacs to correctly indent redef-keyspaces:
;; (put-clojure-indent 'redef-keyspaces 1)

(defmacro redef-keyspaces
  [keyspace-str & exprs]
  `(let [suffixes# {:analytics ""
                    :high-rep "_high_rep"
                    :suffix "_parameters"}
         make-keyspace-name-fn# (fn [suffix-key#]
                                  (fn [] (str ~keyspace-str
                                              (suffix-key# suffixes#))))]
     (with-redefs [c/analytics (make-keyspace-name-fn# :analytics)
                   c/analytics-high-rep (make-keyspace-name-fn# :high-rep)
                   c/parameters (make-keyspace-name-fn# :suffix)]
       ~@exprs)))

(defn create-keyspaces-and-tables
  [keyspace-str]
  (let [s (cs/session)]
    (redef-keyspaces keyspace-str

      ;; create the high_rep keyspace
      ;; (this is from cenx.plataea.cassandra.init/init-keyspaces)
      (dorun (map (fn [statement]
                    (qbits.alia/execute s statement))
                  (->> s
                       (#'ci/calculate-keyspace-factor)
                       (#'ci/non-data-keyspace))))

      ;; create long-term table
      (base/create-table s)

      ;; create processing states table
      (ci/create-proc-states-table s))))

(defn make-prep-select
  [keyspace]
  (let [statement "SELECT state FROM %s.processing_states WHERE state_name = ?;"]
    (fn [session]
      ((c/prepare-once session
                       (format statement keyspace))))))

(defn aggregate
  "Runs the long-term aggregator on the vnf_data table in the given keyspace."
  [keyspace-str]
  (let [;; defined in cd48222.core/dataset-params
        initial-sample-time 1522627200000
        spark-context (-> prod.init/system :spark-manager :spark-context)]
    (redef-keyspaces keyspace-str

      ;; ps/prep-select is private, with-redefs does it anyway :)
      (with-redefs [ps/prep-select (make-prep-select (c/analytics-high-rep))]
        (base/save-latest-timestamp (cs/session) initial-sample-time)
        (agg/populate-long-term-table spark-context (cs/session))))))

;; usage:
;; 1. create the high_rep keyspace, the processing_states table
;; and the long-term table

#_(create-keyspaces-and-tables "upd_par_key_25k_rate_4h")

;; 2. run the aggregator

#_(time (aggregate "upd_par_key_25k_rate_4h"))
