(ns cenx.levski.aggregate-id
  (:require [cenx.plataea.cassandra :as c]
            [cenx.plataea.cassandra.long-term.aggregate :as agg]
            [cenx.plataea.cassandra.long-term.base :as base]
            [cenx.plataea.cassandra.rdd :as rdd]
            [cenx.plataea.cassandra.session :as cs]
            [cenx.plataea.cassandra.vnf :as vnf]
            [cenx.plataea.cassandra.vnf.rdd :as vnf-rdd]
            [cenx.prometheus.spark.cassandra :as spark.cass]
            [qbits.alia :as alia]
            [qbits.hayt :as hayt]
            [sparkling.core :as spark]))

;; put this in levski/src/cenx/levski
;; then add the namespace to the aot list in the project file

;; build and deploy levski, open a repl
;; do this first or there will be a ClassNotFoundException

#_(Class/forName "cenx.levski.aggregate_id__init")

;; aggregate a single id, one hour

#_(cenx.levski.aggregate-id/aggregate-one-id
 (-> prod.init/system :spark-manager :spark-context)
 "twelve_hours"
 "id0"
 1522627200000
 (+ 1522627200000 (* 60 60 1000)))

;; aggregate all ids, one hour

#_(cenx.levski.aggregate-id/aggregate-one-id-at-a-time
 (-> prod.init/system :spark-manager :spark-context)
 "twelve_hours"
 1522627200000
 (+ 1522627200000 (* 60 60 1000)))

(defmacro redef-keyspaces
  [keyspace-str & exprs]
  `(let [suffixes# {:analytics ""
                    :high-rep "_high_rep"
                    :suffix "_parameters"}
         make-keyspace-name-fn# (fn [suffix-key#]
                                  (fn [] (str ~keyspace-str
                                              (suffix-key# suffixes#))))]
     (with-redefs [c/analytics (make-keyspace-name-fn# :analytics)
                   c/analytics-high-rep (make-keyspace-name-fn# :high-rep)
                   c/parameters (make-keyspace-name-fn# :suffix)]
       ~@exprs)))

(defn load-single-id-from-cassandra
  "The fn in cenx.plataea.cassandra.vnf.rdd, modified to use .where and load rows
  for only one id."
  [spark-context id start-ms end-ms]
  (let [id-family "vim"
        week 2517
        ;; either provide these or put the timestamp before the metric
        ;; in the clustering key
        metrics (->> 10
                     range
                     (map #(->> % inc (format "metric%02d"))))]
    (as-> spark-context $
      (spark.cass/table->rdd (c/analytics) vnf/table-name $)
      (.where $
              (str "id = ? AND idfamily = ? AND week = ? AND "
                   "metric IN ? AND timestamp > ? AND timestamp < ?")
              (into-array Object [id id-family week
                                  metrics start-ms end-ms]))
      (rdd/span-by @#'vnf-rdd/key-class @#'vnf-rdd/read-row-fn $)
      (spark/map-values #(map rdd/cassandra-row->map %) $))))

(defn get-ids-in-short-term-table
  [cassandra-session keyspace-str]
  (->> [(hayt/use-keyspace keyspace-str)
        (hayt/select :vnf_data (->> [:id :idfamily :week]
                                    (apply hayt/distinct*)
                                    hayt/columns))]
       (map #(alia/execute cassandra-session %))
       second ;; first is result of use-keyspace, second is result of select
       (map :id)
       set))

(defn aggregate-one-id
  [spark-context keyspace-str id start-ms end-ms]
  (redef-keyspaces keyspace-str
    (let [short-term-rows (load-single-id-from-cassandra spark-context
                                                         id start-ms end-ms)]
      (->> short-term-rows
           (#'agg/aggregate)
           (rdd/save-to-cassandra (:table-name base/long-term-table-schema))))))

(defn aggregate-one-id-at-a-time
  [spark-context keyspace-str start-hour end-hour]
  (let [cassandra-session (cs/session)
        ids (get-ids-in-short-term-table cassandra-session keyspace-str)]
    (->> ids
         (map #(aggregate-one-id spark-context keyspace-str % start-hour end-hour))
         dorun)))
