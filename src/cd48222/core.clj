(ns cd48222.core
  (:require [cd48222.common :as common]
            [qbits.alia :as alia]
            [qbits.hayt :as hayt]))

(def columns-with-updated-partition-key
  (-> common/columns
      (assoc :primary-key [[:id :idfamily :week :metric] :timestamp])
      (assoc :clustering-order [[:timestamp :desc]])))

(defn create-table
  ([session-obj keyspace columns]
   (common/create-table session-obj
                        ;; cassandra doesn't behave well if the keyspace
                        ;; has mixed case
                        (clojure.string/lower-case keyspace)
                        common/table-name
                        columns
                        (* 10 24 60 60)))
  ([session-obj keyspace columns ttl]
   (common/create-table session-obj keyspace common/table-name columns ttl)))

(defn value-at-index
  [sequence index]
  (nth sequence (rem index (count sequence))))

(defn calculate-week
  [time-ms]
  (let [num-ms-in-day (* 24 60 60 1000)
        num-ms-in-week (* 7 num-ms-in-day)
        num-week-days-preceding-epoch 4
        offset-ms (* num-week-days-preceding-epoch num-ms-in-day)]
    (-> time-ms
        (+ offset-ms)
        (quot num-ms-in-week))))

#_(make-rows 2 {"blah" [10]} 10 12 1)
#_(make-rows 1 {"blah" [10 20]} 10 12 1)
#_(make-rows 1 {"blah" [10] "zoot" [3]} 10 12 1)

(defn make-rows
  [ids metrics
   initial-time current-time time-increment]
  (let [current-index (quot (- current-time initial-time) time-increment)]
    (for [id ids
          metric-name (keys metrics)]
      {:id id
       :idfamily "vim"
       :week (calculate-week current-time)
       :metric metric-name
       :timestamp current-time
       :vdouble (-> metrics
                    (get metric-name)
                    (value-at-index current-index))})))

(defn execute-cql-statements
  [session-obj statements]
  (loop []
    (when-not (try
                (dorun (map (partial alia/execute
                                     (common/get-session session-obj))
                            statements))
                true
                (catch Exception e
                  (println "Exception caught")
                  (println (.getMessage e))
                  ;; give false as result to cause a retry
                  false))
      (println "sleeping 1 minute before retrying")
      (Thread/sleep (* 60 1000))
      (println "reconnecting to cassandra")
      (common/reconnect session-obj)
      (recur))))

(defn create-test-data
  "Puts lots of rows on vnf_data."
  [session-obj rows batch-size keyspace]
  (execute-cql-statements session-obj
                          (list* (hayt/use-keyspace keyspace)
                                 (common/batch-of-row-inserts batch-size
                                                              common/table-name
                                                              rows))))

(def dataset-params
  {:num-ids 1500
   :id-prefix "id"
   :metrics {"metric01" [1.0] "metric02" [2.0] "metric03" [3.0]
             "metric04" [4.0] "metric05" [5.0] "metric06" [6.0]
             "metric07" [7.0] "metric08" [8.0] "metric09" [9.0]
             "metric10" [10.0]}

   ;; Apr 2 2018 00:00
   :initial-sample-time-ms 1522627200000

   :increment-ms 1000
   :batch-size 1200

   :progress-log-increments 60})

(defn generate-dataset
  [session-obj keyspace num-seconds]
  (let [;; put the session inside a box
        {:keys [num-ids metrics

                ;; batch-size must divide num-ids * num-metrics
                ;; because partition throws away the remainder of the elements
                batch-size

                increment-ms initial-sample-time-ms
                progress-log-increments]} dataset-params

        final-sample-time-ms (+ initial-sample-time-ms (* num-seconds 1000))
        start-time-ms (System/currentTimeMillis)]

    (loop [current-sample-time-ms initial-sample-time-ms
           num-increments 0]

      (when (= (rem num-increments progress-log-increments) 0)
        (common/log-progress (* num-ids (count metrics) num-increments)
                             (- current-sample-time-ms initial-sample-time-ms)
                             start-time-ms))

      (when (< current-sample-time-ms final-sample-time-ms)
        (let [next-sample-time-ms (+ current-sample-time-ms increment-ms)]
          (create-test-data session-obj
                            (make-rows (map #(str (:id-prefix dataset-params) %)
                                            (range num-ids))
                                       metrics
                                       initial-sample-time-ms
                                       current-sample-time-ms
                                       increment-ms)
                            batch-size
                            keyspace)
          (recur next-sample-time-ms
                 (inc num-increments)))))))

(defn update-sample-time?
  [num-increments num-ids]
  (let [next-num-increments (+ num-increments 1)]
    (= (rem next-num-increments num-ids) 0)))

(defn generate-dataset-with-single-id-batches
  [session-obj keyspace num-seconds]
  (let [{:keys [num-ids metrics batch-size
                increment-ms initial-sample-time-ms]} dataset-params

        ;; num-seconds must be a multiple of number of metrics
        ;; (one batch is: one id, all its metrics, as many seconds as possible)
        num-metrics (count metrics)
        _ (assert (= (rem num-seconds num-metrics) 0))

        sample-seconds-in-increment (quot num-seconds num-metrics)

        ;; number of increments between each progress log
        ;; each increment corresponds to one id
        progress-log-increments 150

        final-sample-time-ms (+ initial-sample-time-ms (* num-seconds 1000))
        start-time-ms (System/currentTimeMillis)]

    (loop [current-sample-time-ms initial-sample-time-ms
           num-increments 0]

      (when (= (rem num-increments progress-log-increments) 0)
        (common/log-progress (* num-increments
                                sample-seconds-in-increment
                                num-metrics)
                             (- current-sample-time-ms initial-sample-time-ms)
                             start-time-ms))

      (when (< current-sample-time-ms final-sample-time-ms)
        (let [id (rem num-increments num-ids)
              next-sample-time-ms (+ current-sample-time-ms
                                     (* sample-seconds-in-increment 1000))]
          (create-test-data session-obj
                            (make-rows [(str "id" id)]
                                       metrics
                                       initial-sample-time-ms
                                       current-sample-time-ms
                                       increment-ms)
                            batch-size
                            keyspace)
          (recur (if (update-sample-time? num-increments num-ids)
                   next-sample-time-ms
                   current-sample-time-ms)
                 (inc num-increments)))))))

(defn try-count-rows-for-id
  [session-obj keyspace-str id-str]
  (let [table (common/table-with-keyspace keyspace-str common/table-name)]
    (try
      (as-> table $
        (hayt/select $
                     (hayt/columns (hayt/count*))
                     (hayt/where [[= :id id-str]
                                  [= :idfamily "vim"]
                                  [= :week 2518]]))
        (alia/execute (common/get-session session-obj) $))
      (catch Exception e
        (println "Exception caught")
        (println (.getMessage e))))))

(defn count-rows-for-id
  [session-obj keyspace-str id-str]
  (let [table (common/table-with-keyspace keyspace-str common/table-name)]
    (loop []
      (let [count (try-count-rows-for-id session-obj keyspace-str id-str)]
        (if-not count
          (do (println "sleeping 1 minute before retrying")
              (Thread/sleep (* 60 1000))
              (println "reconnecting to cassandra")
              (common/reconnect session-obj)
              (recur))
          count)))))

(defn count-rows
  [session keyspace]
  (->> dataset-params
       :num-ids
       range
       (map #(do (when (zero? (rem % 1000)) (println %))
                 (->> %
                      (str "id")
                      (count-rows-for-id session keyspace)
                      first
                      :count)))
       (reduce + 0)))

;;; function that helped Elaine test the short-term migrator

(defn generate-elaines-data
  []
  ;; 8 ids x 5 metrics x 5 timestamps = 200 rows
  (let [rows (for [ts (map #(-> % (* 60 1000) (+ 1533812400000)) (range 5))
                   metric ["A" "B" "C" "D" "E"]
                   id (map #(str "vm" %) (range 8))]
               {:id id :m metric :ts ts})]
    (->> rows
         (map #(assoc % :v (rand-int 100)))
         (map #(vector (:id %) "vim" 2536, (:m %) (:ts %) (:v %) "" ""))
         (map (partial interpose ","))
         (map (partial apply str))
         (map #(str % "\n"))
         (apply str)
         println)))

;; this creates a four-hour table at 25K samples/s
;; with timestamp before metric in the clustering key

#_(def sesh (common/make-session-obj "cassandra01-amun.cenx.localnet"))

#_(create-table sesh
                "upd_cl_key_25k_rate_4h"
                common/columns-with-updated-clustering-key)

#_(with-redefs [dataset-params (assoc dataset-params :num-ids 2500)]
  (generate-dataset sesh "upd_cl_key_25k_rate_4h" (* 4 60 60)))

;; this creates a four-hour table at 25K samples/s
;; with metric in the partition key

#_(create-table sesh
                "upd_par_key_25k_rate_4h"
                columns-with-updated-partition-key)

#_(with-redefs [dataset-params (assoc dataset-params :num-ids 2500)]
  (generate-dataset sesh "upd_par_key_25k_rate_4h" (* 4 60 60)))

;; this creates a table to test the migration code

#_(create-table sesh "migration_test" columns-with-updated-clustering-key)

#_(with-redefs [dataset-params (-> dataset-params
                                 (assoc :num-ids 1000)
                                 (assoc :metrics {"metric01" [1.0]})
                                 (assoc :increment-ms 60000))]
  (generate-dataset sesh "migration_test" (* 10 24 60 60)))

;; table with one day of data at 25K samples/s

#_(create-table sesh
                "upd_cl_key_25k_rate_1d_ttl0"
                common/columns-with-updated-clustering-key
                0)

;; (with restart)

#_(with-redefs [dataset-params (-> dataset-params
                                 (assoc :num-ids 2500)
                                 (update :initial-sample-time-ms
                                         #(+ % (* (+ (* 2 60)  ;; 2 hours
                                                     37)       ;; 37 minutes
                                                  60 1000))))] ;; in milliseconds
  (generate-dataset sesh "upd_cl_key_25k_rate_1d_ttl0" (* 24 60 60)))

;; local test:

;; 1. clear processing_states, short_term and long_term
;; cqlsh -e "delete from devanalytics_high_rep.processing_states where state_name='long_term_latest_timestamp'; truncate devanalytics.short_term; truncate devanalytics.long_term;"

;; 2. populate short-term with one hour of data, starting 25 hours to the past
;; (now is 1:48 pm, I must populate from 12 pm to 1 pm yesterday)
;; 3. start levski, check that the aggregator ran
;; 4. open ui (delorean and epiphany should be running), check the graph
;; 5. truncate short_term, check the graph again

#_(with-redefs [cd48222.common/table-name :short_term
              dataset-params (merge dataset-params
                                    {:num-ids 10
                                     :id-prefix "vm"
                                     :metrics {"laLoadInt" [10.0 20.0 30.0 40.0]}
                                     :increment-ms (* 60 1000)
                                     ;; May 13 2018 12 pm local time
                                     :initial-sample-time-ms 1526227200000})]
  (generate-dataset sesh "devanalytics" (* 60 60)))

;; table with one day of data, 1.5M ids, each with one metric
;; one sample per minute for each id-metric

#_(with-redefs [cd48222.common/table-name :short_term]
  (create-table sesh
                "upd_cl_key_1500k_min_1d_ttl0"
                common/columns-with-updated-clustering-key
                0))

#_(with-redefs [cd48222.common/table-name :short_term
              dataset-params (merge dataset-params
                                    {:num-ids (* 1500 1000)
                                     :metrics {"unique-metric" [10.0]}
                                     :batch-size 900
                                     :increment-ms (* 60 1000)
                                     :progress-log-increments 10})]
  (generate-dataset sesh "upd_cl_key_1500k_min_1d_ttl0" (* 24 60 60)))

;; 3.5M/min table described in CD-41364

#_(def sesh (common/make-session-obj "cassandra01-amun-ssd.cenx.localnet"))

#_(let [;; performance metrics: 70k sites, 4 ids per site, 9 metrics, 1 minute interval
      ;; fudged up from 70k to get perf + util = 3.5M / min
      num-sites 72917
      ;; 3,628,800,000 per day
      perf-params {:id "perf"
                   :num-ids (* num-sites 4)
                   :metrics {"min-fl" [10.0] "avg-fl" [55.0] "max-fl" [100.0]
                             "min-fd" [10.0] "avg-fd" [55.0] "max-fd" [100.0]
                             "min-fdv" [10.0] "avg-fdv" [55.0] "max-fdv" [100.0]}
                   :batch-size 900
                   :increment-ms (* 60 1000)
                   :progress-log-increments 10}
      ;; utilization metrics: 70k sites, 6 ids per site, 10 metrics, 5 minute interval
      ;; 1,209,600,000 per day
      util-params {:id "util"
                   :num-ids (* num-sites 6)
                   :metrics {"m01" [1.0] "m02" [2.0] "m03" [3.0] "m04" [4.0]
                             "m05" [5.0] "m06" [6.0] "m07" [7.0] "m08" [8.0]
                             "m09" [9.0] "m10" [10.0]}
                   :batch-size 900
                   :increment-ms (* 5 60 1000)
                   :progress-log-increments 10}
      ;; lte-ran metrics: 70k sites, 9 ids per site, 20 metrics, 15 minute interval
      ;; 1,209,600,000 per day
      ran-params {:id "lte-ran"
                  :num-ids (* num-sites 9)
                  :metrics {"m01" [1.0] "m02" [2.0] "m03" [3.0] "m04" [4.0]
                            "m05" [5.0] "m06" [6.0] "m07" [7.0] "m08" [8.0]
                            "m09" [9.0] "m10" [10.0] "m11" [11.0] "m12" [12.0]
                            "m13" [13.0] "m14" [14.0] "m15" [15.0] "m16" [16.0]
                            "m17" [17.0] "m18" [18.0] "m19" [19.0] "m20" [20.0]}
                  :batch-size 900
                  :increment-ms (* 15 60 1000)
                  :progress-log-increments 10}
      ;; performance + utilization           = 3.36M per minute
      ;; performance + utilization + lte-ran = 4.2M per minute
      keyspace "cd41364_perf_util_1d_ttl0"]

  (create-table sesh
                keyspace
                common/columns-with-updated-clustering-key
                0)
  (with-redefs [dataset-params (merge dataset-params perf-params)]
    (generate-dataset sesh keyspace (* 24 60 60)))
  (with-redefs [dataset-params (merge dataset-params util-params)]
    (generate-dataset sesh keyspace (* 24 60 60))))

;; this counts the number of rows in the 3.5M/min table

#_(let [num-sites 72917
      util-params {:num-ids (* num-sites 6)}
      keyspace "cd41364_perf_util_1d_ttl0"]
  (with-redefs [dataset-params (merge dataset-params util-params)]
    (count-rows sesh keyspace)))
