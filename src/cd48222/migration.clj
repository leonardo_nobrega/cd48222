(ns cd48222.migration
  (:require [cd48222.common :as common]
            [clojure.core.async :as async]
            [qbits.alia :as alia]
            [qbits.hayt :as hayt]))

(def source-table-name common/table-name)
(def source-columns common/columns)

(def target-table-name :short_term)
(def target-columns common/columns-with-updated-clustering-key)

(def time-to-print-ms 60000)

(defn elapsed-time
  [current-state]
  (->> current-state
       :start-time
       (- (System/currentTimeMillis))
       common/time-ms->string))

(defn percentage
  [current-state]
  (->> (count (:all-ids current-state))
       (/ (inc (.indexOf (:all-ids current-state) (:current current-state))))
       (* 100)
       double
       (format "%.2f")))

(defn print-state
  [current-state]
  (-> current-state
      (assoc :elapsed (elapsed-time current-state))
      (assoc :percentage-of-ids (percentage current-state))
      (update :num-rows common/format-long-number)
      (dissoc :all-ids)
      (dissoc :start-time)
      clojure.pprint/pprint))

(defn start-status-thread
  "Periodically prints the state of a process."
  [progress-channel]
  (async/go-loop [state {}
                  timer (async/timeout time-to-print-ms)]
    (let [[msg ch] (async/alts! [timer progress-channel])]
      (cond
        ;; timed out:
        ;; print state, start a new timer
        (= ch timer)
        (do (print-state state)
            (recur state (async/timeout time-to-print-ms)))

        ;; work is complete:
        ;; print state and return
        (= msg :end) :done

        ;; did not time out and did not finish working
        ;; (received a new state):
        ;; update the state, keep waiting on the same timer
        :else
        (recur msg timer)))))

(defn get-rows-to-migrate
  "Extracts an id and week from the current-state and
  uses them to query the source table."
  [cassandra-session current-state]
  (as-> current-state $
    (:current $)
    (hayt/where $)
    (hayt/select source-table-name $)
    (alia/execute cassandra-session $)))

(defn migrate-one-id
  "Migrates all rows with the same id-week."
  [progress-channel cassandra-session keyspace-str current-state]
  (let [batch-size 100
        rows (get-rows-to-migrate cassandra-session current-state)]

    ;; update the status thread with the current state
    (async/>!! progress-channel current-state)

    ;; write to target table
    (->> rows
         ;; make batches
         (common/batch-of-row-inserts batch-size target-table-name)
         ;; write them
         (list* (hayt/use-keyspace keyspace-str))
         (map #(alia/execute cassandra-session %))
         dorun)

    ;; update num-rows in current-state
    (update current-state :num-rows #(+ (count rows) %))))

(defn get-all-ids
  "Gets all id-week tuples in the table.
  This gives us a way to iterate on the table."
  [cassandra-session keyspace-str]
  (let [table (common/table-with-keyspace keyspace-str source-table-name)]
    (as-> table $
      (hayt/select $ (->> source-columns
                          :primary-key
                          first
                          (apply hayt/distinct*)
                          hayt/columns))
      (alia/execute cassandra-session $)
      ;; sort ids to do them in order
      ;; if a failure happens, we will know from where to restart
      (sort-by (juxt :id :week) $))))

(defn get-all-ids-if-needed
  "Adds the list of all ids to the initial state."
  [cassandra-session keyspace-str initial-state]
  (cond-> initial-state
    (not (contains? initial-state :start-time))
    (assoc :start-time (System/currentTimeMillis))

    (not (contains? initial-state :all-ids))
    (assoc :all-ids (get-all-ids cassandra-session keyspace-str))

    ;; if the initial state has a current value,
    ;; drop id-week tuples from all-ids
    ;; (in case of problems, this lets us start closer to the point where we failed)
    (contains? initial-state :current)
    (update :all-ids #(-> % (.indexOf (:current initial-state)) (drop %)))

    true (assoc :num-rows 0)))

(defn migrate-with-session
  ([cassandra-session keyspace-str]
   (common/create-table cassandra-session
                        keyspace-str
                        target-table-name
                        target-columns)
   (migrate-with-session cassandra-session keyspace-str {}))

  ([cassandra-session keyspace-str initial-state]
   (let [progress-channel (async/chan)
         current-state (get-all-ids-if-needed cassandra-session
                                              keyspace-str
                                              initial-state)]
     (start-status-thread progress-channel)
     (try
       (->> current-state
            :all-ids
            ;; migrates id by id and keeps track of the number of rows written
            (reduce (fn [state id-week]
                      (->> id-week
                           (assoc state :current)
                           (migrate-one-id progress-channel
                                           cassandra-session
                                           keyspace-str)))
                    current-state)
            print-state)
       (catch Exception e
         (println "Exception caught")
         (println e))
       (finally (async/>!! progress-channel :end))))
   ;; give the status thread a little time to print the last state
   (Thread/sleep 100)))

(defn migrate
  "Reads from the source table, writes to the target table."
  [cassandra-address keyspace-str]
  (let [session (common/make-session cassandra-address)]
    (migrate-with-session session keyspace-str)))

(comment
  ;; test code (run on repl)
  (def sesh (cd48222.common/make-session))
  (create-table sesh "migration_test" cd48222.common/columns)
  (with-redefs [dataset-params (assoc dataset-params :num-ids 10)]
    (generate-dataset sesh "migration_test" (* 60 60)))
  (Class/forName "cd48222.migration__init")
  (cd48222.migration/migrate-with-session sesh "migration_test")

  ;; this tests starting from id7
  (cd48222.migration/migrate-with-session sesh
                                          "migration_test"
                                          {:current {:id "id7"
                                                     :idfamily "vim"
                                                     :week 2517}}))
