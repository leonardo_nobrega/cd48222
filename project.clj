(defproject cd48222 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[cc.qbits/alia-all "3.1.11" :exclusions [cc.qbits/tardis]]
                 [cc.qbits/hayt "3.1.0"]
                 [org.clojure/clojure "1.8.0"]
                 ]
  :aot [cd48222.core cd48222.common cd48222.migration]
  :profiles {:repl {:main cd48222.core
                    :target-path "target"}})
